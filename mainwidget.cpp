/*
 * mainwidget.cpp
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#include <iostream>
#include <qlistview.h>
#include <qlayout.h>
#include <qtabwidget.h>
#include <qlistbox.h>
#include <qmenubar.h>
#include <qapplication.h>
#include <qmessagebox.h>
#include <qstatusbar.h>
#include <qtimer.h>
#include <qheader.h>
#include <qaction.h>

#include <qstrlist.h>

#include "kfpm.h"
#include "mainwidget.h"

extern QPtrList<PM_DB> syncs;

MainWidget::MainWidget(QWidget * parent, const char * name, WFlags f) : QWidget (parent, name, f)
{
	QVBoxLayout *topLayout = new QVBoxLayout(this);
	Q_CHECK_PTR(topLayout);

	/* Menubar */
	QPopupMenu *file = new QPopupMenu(this);
	Q_CHECK_PTR(file);

	QAction *ExitAction = new QAction("E&xit", CTRL+Key_X, this, "exit");
	connect(ExitAction, SIGNAL(activated()) , qApp, SLOT(quit()) );
	ExitAction->addTo(file);

	QPopupMenu *pacman = new QPopupMenu(this);
	Q_CHECK_PTR(pacman);
	QAction *UpdateAction = new QAction("&Update syncdb", CTRL+Key_Y, this, "foo");
	connect(UpdateAction, SIGNAL(activated()) , this, SLOT(updateSyncdb()) );
	UpdateAction->addTo(pacman);
	QAction *UpgradeAction = new QAction("&Upgrade packages", CTRL+Key_U, this, "foo");
	connect(UpgradeAction, SIGNAL(activated()) , this, SLOT(upgradePackages()) );
	UpgradeAction->addTo(pacman);

	QPopupMenu *help = new QPopupMenu(this);
	Q_CHECK_PTR(help);

	QAction *AboutAction = new QAction("&About", NULL, this, "about");
	connect(AboutAction, SIGNAL(activated()), this, SLOT(about()) );
	QAction *AboutQtAction = new QAction("About &Qt", NULL, this, "aboutQt");
	connect(AboutQtAction, SIGNAL(activated()), this, SLOT(aboutQt()) );
	AboutAction->addTo(help);
	AboutQtAction->addTo(help);

	QMenuBar *menubar = new QMenuBar(this);
	Q_CHECK_PTR(menubar);
	menubar->insertItem( "&File", file );
	menubar->insertItem( "&Pacman", pacman );
	menubar->insertItem( "&Help", help);

	topLayout->setMenuBar(menubar);
	
	/* Groups */
	GroupsView = new QListView(this);
	Q_CHECK_PTR(GroupsView);
	GroupsView->addColumn("Groups");

	GroupsView->setSelectionMode( QListView::Single );

	connect( GroupsView, SIGNAL( clicked( QListViewItem* ) ), this, SLOT(groupItemSelected(QListViewItem*)) );

	/* Packages */
	PackagesView = new QListView(this);
	Q_CHECK_PTR(PackagesView);
	PackagesView->addColumn("Name");
	PackagesView->addColumn("Status");
	PackagesView->addColumn("Version");

	PackagesView->setSelectionMode( QListView::Single );

	connect( PackagesView, SIGNAL( clicked( QListViewItem* ) ), this, SLOT(packageItemSelected(QListViewItem*)) );

	/* Info */
	InfoView = new QListView(this);
	Q_CHECK_PTR(InfoView);
	InfoView->header()->hide();
	/* Disable sorting
	InfoView->setSorting(-1);*/
	InfoView->setSelectionMode(QListView::NoSelection);
	InfoView->addColumn("Directive");
	InfoView->addColumn("Value");

	/* Details */
	QTabWidget *pkgdetails = new QTabWidget(this);
	Q_CHECK_PTR(pkgdetails);
	pkgdetails->addTab(InfoView, "Info");

	/* Layout */
	QHBoxLayout *leftgrouplist = new QHBoxLayout(topLayout);
	Q_CHECK_PTR(leftgrouplist);
	leftgrouplist->addWidget(GroupsView);

	QVBoxLayout *details = new QVBoxLayout(leftgrouplist);
	Q_CHECK_PTR(details);
	details->addWidget(PackagesView);
	details->addWidget(pkgdetails);

	/* StatusBar */
	StatusBar = new QStatusBar(this);
	Q_CHECK_PTR(StatusBar);

	topLayout->addWidget(StatusBar);
}

MainWidget::~MainWidget()
{
	delete FPMClass;
}

void MainWidget::about() {
	QMessageBox::about(this, "Kfpm for Frugalware",
			"Kfpm is a graphical fronted for libpacman.\n\n"
			"(c) 2007 V�r�sk�i Andr�s");
}

void MainWidget::aboutQt() {
	QMessageBox::aboutQt(this, "About Qt");
}

void MainWidget::updateGroupList() {
	QStrList groupnames = FPMClass->sync_group(FPMClass->getSync());
	for (QStrList::Iterator it = groupnames.begin(); it != groupnames.end(); it++) {
		(void) new QListViewItem(GroupsView, *it);
	}
	setStatusBarMessage("Grouplist loaded.");
}

void MainWidget::updateGroupPackageList(const char *groupname) {	
	QPixmap pm(12, 12);
	pm.fill(Qt::green);
	QStrList *foo = new QStrList();
	foo->append(groupname);
	QValueVector<PM_DB*> dbs = FPMClass->getSync();
	QStrList pkgnames = FPMClass->sync_group(dbs, foo);
	for (QStrList::Iterator it = pkgnames.begin(); it != pkgnames.end(); it++) {
		/* FIXME it should check all sync db, not just the first one */
		PM_PKG *pkg = pacman_db_readpkg(dbs.first(), *it);
		PM_PKG *localpkg = pacman_db_readpkg(FPMClass->LocalDB, *it);

		QString version = (char *)pacman_pkg_getinfo(pkg, PM_PKG_VERSION);

		QString date = (char *)pacman_pkg_getinfo(localpkg, PM_PKG_INSTALLDATE);
		if (date.isEmpty())
			pm.fill(Qt::red);
		else {
			QString localversion = (char *)pacman_pkg_getinfo(localpkg, PM_PKG_VERSION);
			if (localversion != version)
				pm.fill(Qt::yellow);
		}

		QCheckListItem *clitem = new QCheckListItem(PackagesView, *it, QCheckListItem::CheckBox);
		clitem->setPixmap(1, pm);
		clitem->setText(2, version);
	}
}

void MainWidget::slot_initialize() {
	FPMClass = new FPM();
	setStatusBarMessage("Initializing...");
	qApp->processEvents();
	updateGroupList();
}

void MainWidget::setStatusBarMessage(QString message) {
	StatusBar->message(message);
}

void MainWidget::groupItemSelected( QListViewItem *item )
{
	if (item == 0) {
		return;
	}
	else {
		PackagesView->clear();
		updateGroupPackageList(item->text(0));
	}
}

void MainWidget::packageItemSelected(QListViewItem *item)
{
	if (item == 0) {
		return;
	}
	updateFullInfo(item->text(0));
}

void MainWidget::updateFullInfo(const char *pkgname)
{
	if (pkgname == 0) {
		return;
	}
	else {
		char buffer[50]; //for converting sizes
		InfoView->clear();
		QValueVector<PM_DB*> dbs = FPMClass->getSync();

		/* FIXME it should check all sync db, not just the first one */
		PM_PKG *pkg = pacman_db_readpkg(dbs.first(), (char *)pkgname);
		PM_PKG *localpkg = pacman_db_readpkg(FPMClass->LocalDB, (char *)pkgname);

		(void) new QListViewItem(InfoView, "Name", (char *)pacman_pkg_getinfo(pkg, PM_PKG_NAME));

		(void) new QListViewItem(InfoView, "Version", (char *)pacman_pkg_getinfo(pkg, PM_PKG_VERSION));

		long int size = (long int)pacman_pkg_getinfo(pkg, PM_PKG_SIZE);
		sprintf(buffer, "%ld", size);
		(void) new QListViewItem(InfoView, "Size (compressed)", buffer);

		size = (long int)pacman_pkg_getinfo(localpkg, PM_PKG_SIZE);
		sprintf(buffer, "%ld", size);
		(void) new QListViewItem(InfoView, "Size (uncompressed)", buffer);

		(void) new QListViewItem(InfoView, "Description", (char *)pacman_pkg_getinfo(pkg, PM_PKG_DESC));

		QString date = (char *)pacman_pkg_getinfo(localpkg, PM_PKG_INSTALLDATE);
		switch((long)pacman_pkg_getinfo(localpkg, PM_PKG_REASON)) {
			case PM_PKG_REASON_EXPLICIT:
				if(date.isEmpty())
					(void) new QListViewItem(InfoView, "Reason", "Not installed");
				else
					(void) new QListViewItem(InfoView, "Reason", "Explicitly installed");
				break;
			case PM_PKG_REASON_DEPEND:
				(void) new QListViewItem(InfoView, "Reason", "Installed as a dependency for another package");
				break;
			default:
				(void) new QListViewItem(InfoView, "Reason", "Unknown");
				break;
		}
		if (!date.isEmpty())
			(void) new QListViewItem(InfoView, "Install Date", date);

		PM_LIST *depends = (PM_LIST*)pacman_pkg_getinfo(pkg, PM_PKG_DEPENDS);
		(void) new QListViewItem(InfoView, "Depends", FPMClass->PM_LISTtoQString(depends));
	}
}

void MainWidget::updateSyncdb() {
	QValueVector<PM_DB*> dbs = FPMClass->getSync();
	// FIXME - catch expections
	FPMClass->sync_synctree(0, dbs);
}

void MainWidget::upgradePackages() {
	// FIXME -- exceptions
	FPMClass->sync_upgrade();
}

// vim: sw=4 ts=4 noet
