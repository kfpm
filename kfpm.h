/*
 * kfpm.h
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#ifndef KFPM_H
#define KFPM_H

#include <qapplication.h>

#include "kfpm_pacman.h"

class KFPM : public QApplication
{
	public:
		KFPM(int & argc, char ** argv);
};

#endif /* ifndef KFPM_H */

