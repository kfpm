/*
 * mainwidget.h
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <qwidget.h>
#include <qpushbutton.h>
#include <qlistview.h>
#include <qstatusbar.h>

#include "kfpm_pacman.h"

class MainWidget : public QWidget
{
	Q_OBJECT

	public:
		MainWidget(QWidget * parent = 0, const char * name = 0, Qt::WFlags f = Qt::WType_TopLevel);
		~MainWidget();
	public slots:
		void slot_initialize();
	protected:
		QPushButton *button_quit;
	private:
		FPM *FPMClass;
		QStatusBar *StatusBar;
		QString StatusBarMessage;
		QListView *GroupsView;
		QListView *PackagesView;
		QListView *InfoView;
		void setStatusBarMessage(QString);
		void updateGroupList();
		void updateGroupPackageList(const char*);
		void updateFullInfo(const char*);
	private slots:
		void groupItemSelected(QListViewItem*);
		void packageItemSelected(QListViewItem*);
		void about();
		void aboutQt();
		void updateSyncdb();
		void upgradePackages();
};

#endif /* ifndef MAINWIDGET_H */
