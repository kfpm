/*
 * kfpm_pacman.cpp
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#include <iostream>

#include "kfpm_pacman.h"

QValueVector<PM_DB*> pm_dblist;

FPM::FPM(char *root, char *configfile) {
	pacman_init(root, configfile);
	LocalDB = pacman_db_register((char*)"local");
}

FPM::~FPM () {
	pacman_release();
	std::cout << "FPM destructor called" << std::endl;
}

void FPM::setSync(QValueVector<PM_DB*> syncs)
{
	this->Syncs = syncs;
}

QValueVector<PM_DB*> FPM::getSync(void) {
	return Syncs;
}

QStrList FPM::sync_group(QValueVector<PM_DB*> syncs)
{
	QStrList grpnames;
	for(QValueVector<PM_DB*>::iterator it = syncs.begin(); it != syncs.end(); ++it) {
		PM_LIST *lp;
		for(lp = pacman_db_getgrpcache(*it); lp; lp = pacman_list_next(lp)) {
			grpnames.append((char*)pacman_list_getdata(lp));
		}
	}
	return(grpnames);
}

QStrList FPM::sync_group(QValueVector<PM_DB*> syncs, QStrList *targets)
{
	QStrList pkgnames;
	if (targets->isEmpty()) {
		/* FIXME exception */
		return NULL;
	}
	for(char *target = targets->first(); target; target = targets->next()) {
		for(QValueVector<PM_DB*>::iterator jt = syncs.begin(); jt != syncs.end(); ++jt) {
			PM_GRP *grp = pacman_db_readgrp(*jt, target);
			PM_LIST *packages;
			for(packages = (PM_LIST*)pacman_grp_getinfo(grp, PM_GRP_PKGNAMES); packages; packages = pacman_list_next(packages)) {
				pkgnames.append((char*)pacman_list_getdata(packages));
			}
		}
	}
	return(pkgnames);
}

void cb_db_register(char *section, PM_DB *db)
{
	pm_dblist.push_back(db);
}

void FPM::pacman_init(char *root, char *configfile)
{
	pacman_initialize(root);
	/* FIXME - exception handling */
	if(pacman_parse_config(configfile, cb_db_register, "") != 0) {
		std::cerr << "failed to parse config (" << pacman_strerror(pm_errno) << ")" << std::endl;
	}
	setSync(pm_dblist);
}

/* FIXME - do i need this at all?
QStrList FPM::PM_LISTtoQStringList(PM_LIST* pmlist)
{
	QStrList stringlist;
	for(pmlist = pacman_list_next(pmlist); pmlist; pmlist = pacman_list_next(pmlist)) {
		stringlist.append((char*)pacman_list_getdata(pmlist));
	}
	return stringlist;
}*/

QString FPM::PM_LISTtoQString(PM_LIST* pmlist) {
	QString string;
	for (pmlist = pacman_list_first(pmlist); pmlist; pmlist = pacman_list_next(pmlist)) {
		string += (char*)pacman_list_getdata(pmlist);
		string += " ";
	}
	return string;
}

int FPM::sync_synctree(int force, QValueVector<PM_DB*> syncs) {
	std::cout << "FPM::sync_synctree called\n";
	int ret = 0;
	for(QValueVector<PM_DB*>::iterator it = syncs.begin(); it != syncs.end(); ++it) {
		// FIXME - check return values for all db - exceptions
		ret = pacman_db_update (force, *it);
	}
	return ret;
}

int FPM::sync_upgrade() {
	std::cout << "FPM::sync_upgrade called\n";
	// FIXME - pff, quite shitty atm
	if(pacman_trans_init(PM_TRANS_TYPE_SYNC, 0, NULL, NULL, NULL) == -1) {
		std::cout << "pacman_trans_init failed";
		return 1;
	}
	if (pacman_trans_sysupgrade() == -1) {
		std::cout << "pacman_trans_sysupgrade failed";
		return 1;
	}
	if (pacman_trans_release() == -1) {
		std::cout << "pacman_trans_release failed";
		return 1;
	}
	return 0;
}

// vim: sw=4 ts=4 noet
