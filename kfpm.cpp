/*
 * main.cpp
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#include <iostream>
#include <qapplication.h>
#include <qtimer.h>

#include <unistd.h>
#include <sys/types.h>

#include "kfpm.h"
#include "mainwidget.h"

KFPM::KFPM(int & argc, char ** argv) : QApplication(argc, argv)
{
	MainWidget *w = new MainWidget();
	connect(this, SIGNAL(lastWindowClosed()), this, SLOT(quit()) );
	setMainWidget(w);
	w->show();
	QTimer *t = new QTimer(w);
	connect( t, SIGNAL(timeout()), w, SLOT(slot_initialize()) );
	t->start( 0, TRUE );
}

int main( int argc, char **argv )
{
	uid_t uid = geteuid();
	if (uid != 0) {
		std::cout << "You have to be root to run kfpm.\n";
		exit (1);
	}
	return KFPM(argc, argv).exec();
}
