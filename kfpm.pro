# kfpm.pro

TEMPLATE = app
INCLUDEPATH += .

HEADERS += kfpm.h kfpm_pacman.h mainwidget.h
SOURCES += kfpm.cpp kfpm_pacman.cpp mainwidget.cpp
CONFIG += qt thread warn_on debug
LIBS += -lpacman
