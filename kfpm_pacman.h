/*
 * kfpm_pacman.h
 *
 * Copyright (c) 2007 VOROSKOI Andras <voroskoi@frugalware.org>
 *
 * Copyright: See COPYING file that comes with this distribution.
 *
 */

#ifndef KFPM_PACMAN_H
#define KFPM_PACMAN_H

#include <pacman.h>

#include <Qt3Support/q3valuevector.h>
#include <QStringList>

#define ROOTDIR "/"
#define PACCONF "/etc/pacman.conf"

class FPM {
	private:
		QString PacmanConfig;
		Q3ValueVector<PM_DB*> Syncs;
		void pacman_init(char*, char*);
	public:
		FPM(char *root = (char *)ROOTDIR, char *configfile = (char *)PACCONF);
		~FPM();
		PM_DB *LocalDB;
		void setSync(Q3ValueVector<PM_DB*>);
		Q3ValueVector<PM_DB*> getSync(void);
		QStringList sync_group(Q3ValueVector<PM_DB*>);
		QStringList sync_group(Q3ValueVector<PM_DB*>, QStringList*);
		QString PM_LISTtoQString(PM_LIST*);
		int sync_synctree(int, Q3ValueVector<PM_DB*>);
		int sync_upgrade();
};

#endif /* ifndef KFPM_PACMAN_H */

// vim: sw=4 ts=4 noet
